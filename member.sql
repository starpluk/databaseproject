-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2020 at 10:40 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `member`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_product`
--

CREATE TABLE `book_product` (
  `Pbook_ID` int(11) NOT NULL,
  `Pbook_name` varchar(50) NOT NULL,
  `Pbook_detail` varchar(100) NOT NULL,
  `Pbook_price` int(11) NOT NULL,
  `Pbookty_ID` int(11) NOT NULL,
  `Pbook_img` varchar(50) NOT NULL,
  `Pbook_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_product`
--

INSERT INTO `book_product` (`Pbook_ID`, `Pbook_name`, `Pbook_detail`, `Pbook_price`, `Pbookty_ID`, `Pbook_img`, `Pbook_num`) VALUES
(1, 'EL321', 'ชีทวิชา EL321', 35, 32, 'pic/EL321.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book_type`
--

CREATE TABLE `book_type` (
  `Btype_name` varchar(50) NOT NULL,
  `Btype_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `Order_ID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Order_Date` date NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `Order_detail` int(11) NOT NULL,
  `Order_ID` int(11) NOT NULL,
  `Probook_ID` int(11) NOT NULL,
  `Order_Qty` int(11) NOT NULL,
  `Pbook_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`Order_detail`, `Order_ID`, `Probook_ID`, `Order_Qty`, `Pbook_name`) VALUES
(35, 1, 1, 1, 'EL321'),
(35, 7, 1, 1, 'EL321');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `Pay_ID` int(11) NOT NULL,
  `Order_ID` int(11) NOT NULL,
  `Pay_date` date NOT NULL,
  `sum_price` int(11) NOT NULL,
  `Bank_name` varchar(50) DEFAULT NULL,
  `User_tel` int(11) NOT NULL,
  `Pay_time` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`Pay_ID`, `Order_ID`, `Pay_date`, `sum_price`, `Bank_name`, `User_tel`, `Pay_time`) VALUES
(1, 1, '2020-05-30', 35, 'กสิกรไทย', 24213213, NULL),
(2, 1, '2020-05-17', 35, 'ทหารไทย 2-554-898-6', 956569681, NULL),
(3, 1, '2020-05-17', 35, 'ทหารไทย 2-554-898-6', 956569681, NULL),
(4, 1, '2020-05-17', 35, 'ไทยพาณิชย์ 44-554-898-6', 956569681, NULL),
(5, 1, '2020-05-17', 35, 'ไทยพาณิชย์ 44-554-898-6', 0, NULL),
(6, 1, '2020-05-17', 35, 'ไทยพาณิชย์ 44-554-898-6', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userdetail`
--

CREATE TABLE `userdetail` (
  `User_ID` int(11) NOT NULL,
  `User_name` varchar(50) NOT NULL,
  `User_lname` varchar(50) NOT NULL,
  `User_addr` varchar(100) NOT NULL,
  `User_email` varchar(50) NOT NULL,
  `User_tel` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdetail`
--

INSERT INTO `userdetail` (`User_ID`, `User_name`, `User_lname`, `User_addr`, `User_email`, `User_tel`, `Username`, `password`) VALUES
(1, 'vee', 'kae', 'asdd', ' starplukz@gmail.com ', '805044440', 'pluk', '1234'),
(2, 'vee', 'kae', 'asdasd', 'star', '098099', 'plukz', '1234'),
(3, 'vee', 'kae', 'asas', 'starplukz@gmail.com', '0805044440', '', ''),
(4, 'ppp', 'aaa', 'sss', 'starpluk', '080501111', 'pluk11', '12345'),
(5, 'vee', '', 'dddd', 'starplukz@gmail.com', '123456', 'sss', '1234'),
(6, 'vee', '', 'aaasss', 'starplukz@gmail.com', '0805044440', 'starpluk', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_product`
--
ALTER TABLE `book_product`
  ADD PRIMARY KEY (`Pbook_ID`);

--
-- Indexes for table `book_type`
--
ALTER TABLE `book_type`
  ADD PRIMARY KEY (`Btype_ID`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`Order_ID`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`Order_ID`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`Pay_ID`);

--
-- Indexes for table `userdetail`
--
ALTER TABLE `userdetail`
  ADD PRIMARY KEY (`User_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_product`
--
ALTER TABLE `book_product`
  MODIFY `Pbook_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `book_type`
--
ALTER TABLE `book_type`
  MODIFY `Btype_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `Order_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `Order_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `Pay_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `userdetail`
--
ALTER TABLE `userdetail`
  MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
